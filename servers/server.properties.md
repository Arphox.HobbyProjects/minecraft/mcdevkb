# `server.properties`
### Links
- [server.properties – Official Minecraft Wiki](https://minecraft.gamepedia.com/Server.properties)
- [[GUIDE] Server Optimization](https://www.spigotmc.org/threads/guide-server-optimization%E2%9A%A1.283181/)

## My recommendations
**If you can't find an option in this list that is because I recommend using the default value.**
- `network-compression-threshold`: The default 256 is a balanced value. Since network compression is multi-threaded, this should not matter much on a modern CPU.
  **If** your CPU has less than or equal to 2 logical cores, set this to 512. Otherwise, leave it on the default.
- `player-idle-timeout`: Do not leave on 0. Value is in minutes. A value of 15 or 30 sounds good.