# [Paper](https://papermc.io/)
[Below are interesting settings:](https://paper.readthedocs.io/en/latest/server/configuration.html)
## Global settings
- **async-chunks:** can load and save chunks asynchronously ([theoretically](https://www.spigotmc.org/wiki/about-spigot/) CraftBukkit already knows this)
- load-permissions-yml-before-plugins
- enable-player-collisions
- spam-limiter: for "tab-spam"
- book-size
- messages: override various automatic messages by the server (no-permission, kick, etc)

## World settings
- per-player-mob-spawns
- portal-search-radius: nether portal max range to search for existing protal
- enable-treasure-maps: Allows villagers to trade treasure maps
- experience-merge-max-value: to avoid (by default) "huge" merged xp orbs
- **prevent-moving-into-unloaded-chunks**: server will prevent players from moving into unloaded chunks or not
- max-auto-save-chunks-per-tick
- max-entity-collisions
- duplicate-uuid-resolver (+duplicate-uuid-saferegen-delete-range)
- disable-thunder: disables thunderstorms
- skeleton-horse-thunder-spawn-chance
- disable-ice-and-snow
- **keep-spawn-loaded-range**: The range in chunks around spawn to keep loaded.
- **auto-save-interval**
- bed-search-radius
- **use-faster-eigencraft-redstone**: major redstone optimization
- fix-zero-tick-instant-grow-farms
- allow-non-player-entities-on-scoreboards
- disable-explosion-knockback
- baby-zombie-movement-modifier
- game-mechanics
  - **disable-pillager-patrols**
  - disable-chest-cat-detection
  - shield-blocking-delay
  - disable-end-credits
  - disable-player-crits
- max-growth-height (cactus, reeds)
- fishing-time-range
- despawn-ranges
- **anti-xray**