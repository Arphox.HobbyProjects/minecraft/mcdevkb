# [Spigot](https://www.spigotmc.org/)
[About Spigot](https://www.spigotmc.org/wiki/about-spigot/)  
[spigot.yml](https://www.spigotmc.org/wiki/spigot-configuration/)  
[Commands and Permissions](https://www.spigotmc.org/wiki/spigot-commands/)  
[Tips, Tricks & Tutorials](https://www.spigotmc.org/wiki/tips-tricks-tutorials/)  
  - [Vector Programming for Beginners](https://www.spigotmc.org/wiki/vector-programming-for-beginners/)  