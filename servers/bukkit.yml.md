# `bukkit.yml`
## Links
- [Bukkit.yml - Official BukkitWiki](https://bukkit.gamepedia.com/Bukkit.yml)
- [Optimizing bukkit.yml to Reduce Lag - Knowledgebase - Shockbyte](https://shockbyte.com/billing/knowledgebase/154/Optimizing-bukkityml-to-Reduce-Lag.html)
- [[GUIDE] Server Optimization](https://www.spigotmc.org/threads/guide-server-optimization%E2%9A%A1.283181/)

## My recommendations
**If you can't find an option in this list that is because I recommend using the default value.**
- `settings`
  - `query-plugins`: **`false`**; it's better if you hide the list of your plugins from others.
  - `connection-throttle`: Set `0` if you use a proxy (e.g. BungeeCord). Otherwise, leave on default.
- `chunk-gc`
  - `period-in-ticks`: I recommend lowering this value, `400` sounds good.
- `ticks-per`: This section is great for reducing the frequency of entity spawns. Like others, I also recommend lowering these values.
  - `monster-spawns`: 2
  - `water-spawns`: 2
  - `ambient-spawns`: 1