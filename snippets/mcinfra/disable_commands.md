# How to disable commands?

## Disable completely - add empty alias
Edit `commands.yml` to disable commands `plugins` and `pl`:
```
aliases:
  plugins: []
  pl: []
```

**But this means nobody can use these commands, even the admins, ops, or the console.**