Define a `CommandExecutor`:
```java
public class CommandKit implements CommandExecutor {

    // This method is called, when somebody uses our command
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return false;
    }
}
```
- consider manually checking for permissions
- "**Label** represents the exact first word of the command (excluding arguments) that was entered by the sender"


Add it to `plugin.yml`:
```java
# Replace "kit" with the name of your command.
commands:
   kit:
      description: Your description
      usage: /<command>
      permission: yourplugin.yourcommandpermission
```
- I think it is not mandatory to specify a permission here, but I think this is what makes the server register this permission when using e.g. LuckPerms, so this is why you can select your permission from a list. If you just check the permission from code, the server won't know about your permission.


Register it on `onEnable`:
```java
@Override
public void onEnable() {
    // Register our command "kit" (set an instance of your command class as executor)
    this.getCommand("kit").setExecutor(new CommandKit());
}
```

sources:
- https://www.spigotmc.org/wiki/create-a-simple-command/
