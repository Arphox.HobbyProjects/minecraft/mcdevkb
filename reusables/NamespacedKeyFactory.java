package arphox.axcollectorhoppers.reusables;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.NamespacedKey;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

@Singleton
public final class NamespacedKeyFactory {
    private final Plugin plugin;

    @Inject
    NamespacedKeyFactory(Plugin plugin) {
        this.plugin = plugin;
    }

    @NotNull
    public NamespacedKey create(@NotNull String key){
        return new NamespacedKey(plugin, key);
    }
}
