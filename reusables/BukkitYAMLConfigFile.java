package arphox.axcollectorhoppers.config;

/* Note:
    Insert the example .yml file into src/main/resources,
    because when the config is loaded first,
    the example file is used as configuration.
*/

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;

public final class BukkitYAMLConfigFile {
    // based on: http://bukkit.gamepedia.com/Configuration_API_Reference

    private final Plugin plugin;
    private final String fileName;
    private final File file;
    private FileConfiguration fileConfig = null;

    /**
     * Initializes a configuration file. Details: creates a folder for the plugin
     * if needed, then loads the configuration if exists, or loads the config
     * from the src/main/resources folder and copies it to the plugin's folder.
     *
     * @param plugin   The runner plugin's instance
     * @param fileName The file to handle, e.g. "someConfig.yml"
     * @throws IllegalArgumentException If any parameter is null.
     * @apiNote If the fileName doesn't end with ".yml", it is appended.
     */
    public BukkitYAMLConfigFile(Plugin plugin, String fileName) {
        if (plugin == null) {
            throw new IllegalArgumentException("The plugin instance cannot be null.");
        }
        this.plugin = plugin;

        if (fileName == null) {
            throw new IllegalArgumentException("The file name cannot be null.");
        }
        if (!fileName.endsWith(".yml")) {
            fileName += fileName + ".yml";
        }
        this.fileName = fileName;
        file = new File(this.plugin.getDataFolder(), this.fileName);

        createPluginFolderIfNeeded();
        loadDefaultsIfFileDoesNotExist(plugin);
        fileConfig = YamlConfiguration.loadConfiguration(file);
    }

    public FileConfiguration getConfig() {
        return fileConfig;
    }

    public void saveConfig() {
        try {
            fileConfig.save(file);
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + file, ex);
        }
    }

    public void reloadConfig() {
        fileConfig = YamlConfiguration.loadConfiguration(file);
    }

    private void createPluginFolderIfNeeded() {
        File dir = plugin.getDataFolder();
        if (!dir.exists()) {
            boolean success = dir.mkdirs();
            if (!success) {
                throw new RuntimeException("Could not create plugin folder");
            }
        }
    }

    private void loadDefaultsIfFileDoesNotExist(Plugin plugin) {
        if (!file.exists()) {
            loadDefaults();
            plugin.saveResource(fileName, false);
        }
    }

    private void loadDefaults() {
        fileConfig = YamlConfiguration.loadConfiguration(file);

        // Look for defaults in the jar
        Reader defConfigStream = new InputStreamReader(plugin.getResource(fileName), StandardCharsets.UTF_8);
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);

        fileConfig.setDefaults(defConfig);
    }
}
