package arphox.axcollectorhoppers.common;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * Remember that metadata is NOT persisted and is lost on server stop/restart.
 */
@Singleton
public final class MetadataProxy {
    private final Plugin plugin;
    private final String prefix;

    @Inject
    MetadataProxy(@NotNull Plugin plugin) {
        Objects.requireNonNull(plugin);
        this.plugin = plugin;
        prefix = this.plugin.getName() + ".";
    }

    public boolean hasMetadata(@NotNull Metadatable metadatable, @NotNull String metadataKey) {
        Objects.requireNonNull(metadatable);
        Objects.requireNonNull(metadataKey);

        String newKey = prefix + metadataKey;
        return metadatable.hasMetadata(newKey);
    }

    public void removeMetadata(@NotNull Metadatable metadatable, @NotNull String metadataKey) {
        Objects.requireNonNull(metadatable);
        Objects.requireNonNull(metadataKey);

        String newKey = prefix + metadataKey;
        metadatable.removeMetadata(newKey, plugin);
    }

    public void setMetadata(@NotNull Metadatable metadatable, @NotNull String metadataKey, @NotNull Object value) {
        Objects.requireNonNull(metadatable);
        Objects.requireNonNull(metadataKey);
        Objects.requireNonNull(value);

        FixedMetadataValue newMetadataValue = new FixedMetadataValue(plugin, value);
        String newKey = prefix + metadataKey;
        metadatable.setMetadata(newKey, newMetadataValue);
    }

    public @NotNull <T> Optional<T> getMetadata(@NotNull Metadatable metadatable, @NotNull String metadataKey) {
        Objects.requireNonNull(metadatable);
        Objects.requireNonNull(metadataKey);

        String newKey = prefix + metadataKey;
        List<MetadataValue> values = metadatable.getMetadata(newKey);
        if (values.size() == 0)
            return Optional.empty();

        if (values.size() == 1)
            return getMetadataForOneValue(values.get(0));

        return getMetadataForManyValues(values);
    }

    private <T> Optional<T> getMetadataForOneValue(MetadataValue value) {
        Plugin owningPlugin = value.getOwningPlugin();
        if (owningPlugin == null)
            return Optional.empty();

        boolean isOwnedByThisPlugin = owningPlugin.getName().equals(plugin.getName());
        if (!isOwnedByThisPlugin)
            return Optional.empty();

        return Optional.of(castToTypeOfT(value.value()));
    }

    private <T> Optional<T> getMetadataForManyValues(List<MetadataValue> values) {
        Optional<MetadataValue> value = values.stream()
                .filter(x -> x.getOwningPlugin() != null)
                .filter(x -> x.getOwningPlugin().getName().equals(plugin.getName()))
                .findFirst();

        return value.map(this::castToTypeOfT);
    }

    private <T> T castToTypeOfT(Object value) {
        // If it breaks, let it break, stack trace will hopefully help to find the issue.
        //noinspection unchecked
        @SuppressWarnings("unchecked") T result = (T) value;
        return result;
    }
}
