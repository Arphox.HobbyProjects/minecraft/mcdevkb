# PacketPlayOutCommands

Contains data about the list of available root commands for the player.  
Before sending out this packet, the server fires the [`PlayerCommandSendEvent`](articles/events/PlayerCommandSendEvent.md) where it is possible to _remove_ (but not add) entries from the command list.

_Probably_ this packet is used from v1.13+.

I have found two places where the list of available commands sent to the player is:  
(searched for `"getCommandDispatcher"` and looked for parameters of type `EntityPlayer` or similar)
1. `Player#updateCommands()`: this isn't called anywhere in the code, its just public API
2. `NMS.PlayerList#a(EntityPlayer entityplayer, int i)`: this is most likely the point where the player is joining the server, since `PacketPlayOutEntityStatus` is getting sent on the previous lines and based on [`ProtocolLib FAQ`](https://wiki.vg/Protocol_FAQ), this is what comes before PacketPlayOutCommands.

[Here](https://hub.spigotmc.org/stash/projects/SPIGOT/repos/craftbukkit/browse/nms-patches/CommandDispatcher.patch#131)
you can find the patch that applies this event functionality.

**Seealso:**  [How to send/listen PacketPlayOutCommands with ProtocolLib](snippets/mcdev/protocollib/PacketPlayOutCommands.md)