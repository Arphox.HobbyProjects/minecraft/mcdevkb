`public interface Metadatable`

- _"This interface is implemented by all objects that can provide metadata about themselves."_
- this store is a memory-only storage for the purpose of storing additional, non-persistent data for some game objects (Block, Entity, Player, World, etc.)
- since it is not persisted, it is recommended to use only for temporary storage
- if you want to use it only in your plugin, use a key that is properly prefixed with some unique text, like your plugin name. In this case, when you are using the `getMetadata` method, you can expect at most 1 elements in the answer.


Since the API is not the clearest, here is how different method works:

### setMetadata
**Sets** the current item stored in metadata (if exists, overwrites).  
The key is made the following:
1. The subject's (Block, Entity, Player, World, ...) unique identifier (for players, it is `player.getUniqueId()`)
1. The provided `metadataKey` value by the caller
1. The provided `Plugin` instance by the caller


### getMetadata
Returns the stored metadata with the given key, **unfiltered** by plugins (so you get values for other plugins too with the same key).


### hasMetadata
Checks if metadata is stored with the given key, **unfiltered** by plugins (so you get values for other plugins too with the same key).

### removeMetadata
Removes the current item stored in the metadata for the provided valuse of `metadataKey` and `Plugin`.


If you are curious about the implementation, look for this class: `public abstract class MetadataStoreBase<T> {` (`package org.bukkit.metadata;`), or check this snapshot: **[MetadataStoreBase]**







[MetadataStoreBase]: https://topaz.github.io/paste/#XQAAAQDwGwAAAAAAAAA4GEiZzRd1JAg64OGIvtFFWDALBNQncQad0aDGQkh+Gv7JaDpYCwSgmSAwmFGPtyrc3Gcgl2v4W1B7fOPbUjn7tPhy6023LuVTYbYX3V/w4MxiZDOuhUDWTgdPWFpSm/lNysuxWhAWNLHcMoSHuhwGkEs2H/Q0YIZVX14eHgiC2KnHFmJ0AIBEEu+r8/9OPJyHjZUAd9ccvuusKxCNhsCMdSrxUDHP8Q4vyPVX8XrWHiRTOh5GGbtOaUzZsdYJTTswScvU8o6PBKmfq3eNHqsqvAt4SS+vLSylBJufGFCxt3aFYGtAOzGhXplI4cEB9SV+wMVTpRrXZNupqStIWuJqT89qsddopuv2M3QwCy2cys8puTnHq8q6Isi+TnRBvY5jVQkMwyTCUiWm4bzfh3w53kZ5sQ4G7jPliOUT08jMsB9Rn073jDP+qn55EA058qIIrVKDSOpIVNQ6gm3R36nam6tNWUXkMOc/rSuHFZGGsC5jTK1jMVZ219WE1HvbgKPnUaPa0m0jTs38I4xytSyHNmEW1jQdeISqxeUBkwHJpvDNQ5hTQ6KnpcYUs10mAg4+WR6yb1cMxLqaUA5K+ej/qpu7JjbCnwaszvcUpmwCPPnNIzAcMLlTA24H0M5C+vUCPx9PxZcTtrV3nHro0PLQogpTO+4elbCJz352SAm4tGpj+pgAFql2GnUm+N4R8x+6HweC5ruSYSp5W033fGNLtR6+mpF3C2jdAObnVjexktU9xwnbhjaAN9z1PBnRxmsbkqRUvAo10G5/GU/YjXrn54SyoM07+0ZMhpZ1KA788m+eyaGscDap+03v2jyut+034cWeGT+xeGOUZWHK3a10Ul3oYtDTzBqEyrieAT6mJkXCEr/SIhI7/UqiUJLtb9VY6Rk1cspqjKpBIShJ/Jt/EPz4G8pAe7bFlhYSW+IhxGlaHh5LD3YswvnmvSVPnaN/oDgX7Y3hzF5jXP0/F1SFUeRgijqwGOT1yllsiYZ6gbCDaLZuna5p6HZiBUxcP6djDt2kc/jAm3nkzPWBACftjrdJTc35NaLsaK4IiVtSENIDHy3JTwWmUtHBlJHH3iU8DscSBF0ZEg0EYwnYIkKK5w3exGZzjElEmg7mDYyfGrUI1knJbVDQdarCyBfPDTEw6kxloZbZHvV3laF31WwL08ZOdlBDCXUwECww2FkNTzY+jDM2B2qqfN5hkcIUkpEzj7FkIOgSzzryOX2ySvhG9nLUP0hukwRyCHCjbsWgDRx4steXQyNxvVHZdR9aqEo16pVCQBXyZKg2yISwDCDLc8BlAcXroDf3jmYlZ+kzbpGB8mHAE4L8iH4pRYEk2sWXfShH8OF1nUr3MFksIRK5MZgjo+KaNwvYHFBf+8UEmN4MDaFu4+kuchVSqVI4mmqlH09b/IcRy4FX5N/lDYG0tvQAlxVRLU9i1gNk0nJd30q29ljMvJypC1O4irld1wLQVUZ/qXJ6TSru3ExWMtTTaSVzelr6D/g0fHfK2zyO3zJkdxuAeQlDyyXWofGIG8v8qeUICz7Z5dchlbA7UmY+N55Rojtm+CEAhbDRAYx5pFOarDA8X3ZPFaZ0eLfoljj3cbPXhJn5Xz6VPHC4dNREHXvGAnLvRibo/sne+WBo90ptQBe8FAI4djhBqmuuLBgasHXg1NKetgXy5NLIlIW5dczT18oKFfYII4LUhJPmIMm7B5myiyH2CDKRP4wUopdAZRVaKPnO6NnqRukofQM9lpknxMnSjPjfhSccfshAS32zfHQHw3nwN8vQal9u1jLaIt/1AqfMOsjYd7dUxylx07QRHEEIJX6UF1muv26w7JDQCjpgLk1dnoGxN54W5DIIzbyl4jM3B75B9+l7JzRnujLCFcmCZJ+NYovVG69ZBHHmLj2u3eL2D8IiVXVcpvzB61R54yfKYFSRVsfVFZz7K+Eiy5ngwZsFN8tpL9f23gbSGFOmiMq6NZWOi1JzsjxufYVeOSRkiW0278UaaefaH955gHtuZbQBHHRklLefhYsmOK0U0xk3CoC3Ws/auZzsS92h+IxSR9Hq8JrhkY8nwiDl4lYbM0pKWF74Udvu/DUqK5ADYO77fXc9p2vVbPXUYDAkR0gKQxQI88xW6ZSGapEvFs7wslW0qCMM51Q21XYJz7jn6sBbQM8Y0s9//RaBjrY7bdcF7ix0eJtAiNAdb392A5WGhex45UY9g5mKqUkE7kSjDt4xDmFVCcVk3JEHN1/V9Nur6Ye1UvqdYVy5aWbEzcIrRpKWrTG5ecUtlsProdUg6enb1Y6oqGbF+Jff/aw+oYe+BXpifVVsVtcTvuR3iY0DXo992qwFfdCjWb2Of5ZqtE6/PdcZ05WZNt3B7wHhSxSLZdd6ac8ixSiE/1vNNI+GwkahBOYnS8/W0JboejMyw6bXApuczjVvXJ2Ft5O51dnQl1w6UaTEbItY0t4G9vKE339l2Y0zu4lBJborWMd0CtOBuSlHjkwjDlI3FYOd/R/5ow==
