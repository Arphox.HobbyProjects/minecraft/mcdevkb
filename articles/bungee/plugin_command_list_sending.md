# Plugin command list sending on BungeeCord

# Goals

The goal of this article to answer the following question:
- why bungee plugin commands cannot be hidden from players from Spigot server side? (list of available commands on chat, e.g. at `/<TAB>`)
- why bungee sends plugin commands the player does not have permission for
- how can we solve it from Bungee side

## Question #1: _"Why bungee plugin commands cannot be hidden/seen from Spigot server side code?"_
The answer is simple and unfortunate for us: because it is sent by the proxy and does not go through the server.

_I think_ it works like this:
1. The client _somehow_ asks the list of available commands from the "server"
2. The proxy receives the request, forwards it to the server instance
3. The server instance assembles the response, and sends it back
4. The response goes through the proxy (since it is a proxy), and when it receives the response, it adds its own plugin commands, **then** it forwards it to the client.

This makes sense for me and understandable. So we need a proxy-side solution.

## Question #2: _"Why bungee sends such plugin commands the player does not even have permission for?"_
Actually, the answer is rather simple here, too: because the permissions are not set up properly.  
But let's dive into code to see how it really works:

Let's start from the plugin's perspective. I am going to use [AdvancedBan](https://www.spigotmc.org/resources/advancedban.8695/)'s 
code as an example simply because this is the most popular plugin for BungeeCord proxy when I am writing this article.  
Its code can be found [here](https://github.com/DevLeoko/AdvancedBan). I think its code base is horrible in terms of software quality but that's another story.  
Let's see how the usual `ban-ip` command is registered:
```java
mi.setCommandExecutor("ban-ip");
```
which does the following:
```java
@Override
public void setCommandExecutor(String cmd) {
    ProxyServer.getInstance().getPluginManager().registerCommand(getPlugin(), new CommandReceiverBungee(cmd));
}
```
where the `CommandReceiverBungee` class' constructor looks like this:
```java
public CommandReceiverBungee(String name) {
        super(name);
    }
```

Okay, we understand. So it uses the `PluginManager`'s `registerCommand(Plugin plugin, Command command)` method to register its command.

That command is implemented the following way in the BungeeCord code:
```java
public void registerCommand(Plugin plugin, Command command)
{
    commandMap.put( command.getName().toLowerCase( Locale.ROOT ), command );
    for ( String alias : command.getAliases() )
    {
        commandMap.put( alias.toLowerCase( Locale.ROOT ), command );
    }
    commandsByPlugin.put( plugin, command );
}
```
So it justs inserts the command's lowercased name (key) to its `commandMap` with the `command` as a value, and does the same with the command's aliases. Clear.

Let's see the way BungeeCord sends the list of available commands to a client.

We saw earlier that PluginManager stores the `commandMap` and this is what we should track down.  
For us, this is the only method what seems intersting now:
```java
public Collection<Map.Entry<String, Command>> getCommands() {
    return Collections.unmodifiableCollection( commandMap.entrySet() );
}
```
Cool, let's see where it is called! There is only one place: in `DownStreamBridge`'s `void handle(Commands commands)` method:
```java
@Override
public void handle(Commands commands) throws Exception
{
    boolean modified = false;

    for ( Map.Entry<String, Command> command : bungee.getPluginManager().getCommands() )
    {
        if ( !bungee.getDisabledCommands().contains( command.getKey() )
                && commands.getRoot().getChild( command.getKey() ) == null
                && command.getValue().hasPermission( con ) )
        {
            LiteralCommandNode dummy = LiteralArgumentBuilder.literal( command.getKey() )
                    .then( RequiredArgumentBuilder.argument( "args", StringArgumentType.greedyString() )
                            .suggests( Commands.SuggestionRegistry.ASK_SERVER ) )
                    .build();
            commands.getRoot().addChild( dummy );

            modified = true;
        }
    }

    if ( modified )
    {
        con.unsafe().sendPacket( commands );
        throw CancelSendSignal.INSTANCE;
    }
}
```

Okay, this is not that tiny.
I think this is the method that is responsible for sending commands to players. 
From just the code, I don't see how and when this method is called, but it is sure that it is the only place it has to happen. 
In the code we can see some things:

- this method probably potentially modifies the response of the spigot server, and if there is any command that should be included (if modified is true), it cancels the original packet and sends a packet which is extended with its own plugin commands (this logic is at the end of the method)
- this method only sends commands which the player has the permission to see

The latter is interesting. Why doesn't it work, then!?

Let's see how it checks permissions:
```java
public boolean hasPermission(CommandSender sender) {
    return permission == null || permission.isEmpty() || sender.hasPermission( permission );
}
```

Okay, so if permission is null or empty then it returns `true`. There is the permission check as well, but _if there is an actual permission_ to check against.  
What's `permission`, then?
```java
private final String permission;
```
Okay, where does it come from? From the constructor:
```java
public Command(String name, String permission, String... aliases)
{
    Preconditions.checkArgument( name != null, "name" );
    this.name = name;
    this.permission = permission;
    this.aliases = aliases;
}
```

So basically if we give the command a permission, it will have a permission to check.

Let's see AdvancedBan's example again, specifically the `CommandReceiverBungee` class' constructor:
```java
public CommandReceiverBungee(String name) {
        super(name);
    }
```

Yes, there is the problem. The permission is not given for the command.

Instead, the plugin checks permissions on his own, I think here:
`CommandManager#onCommand`: (part)
```java
String permission = command.getPermission();
if (permission != null && !Universal.get().hasPerms(sender, permission)) {
    MessageManager.sendMessage(sender, "General.NoPerms", true);
    return;
}
```

So to sum up:  
Bungee includes the `ban-ip` command in the list of available commands for a client because the Command object does not have a permission set.

I think there _may_ be reasons not to set permissions for a command if it's a mega-command with multiple sub-commands (args) with different permissions.  
But I question the followings:
- is it really a good idea to have such commands?
- is there a way to properly set permissions for sub-commands aka arguments?

Maybe the way to go is to use hierarchical permissions e.g. `plugin.command.subcommand` and somehow set these commands properly, 
but I'm not into permissions to give an authoritive answer on that.

But what my guess is that average plugin developers are simply not good developers enough to either care about these problems or give proper solutions for these problems.

This attitude is also clear if a professional developer's eye scans through plugins' codes.

But if you are a plugin developer and you read this: do not take it personally! Instead, strive to improve! :-)

## Question 3: _"How can we solve it from Bungee side?"_
The simple answer is: it's the plugin author's job.

But I would like to find another way since I cannot guarantee that plugin authors will do this or can do this on their commands.