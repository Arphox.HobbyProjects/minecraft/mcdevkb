# [PlayerCommandSendEvent](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/player/PlayerCommandSendEvent.html)

**Description:** Gives the opportunity to modify the list of root commands sent to a player.

From API docs:  
_"This event is called when the list of available server commands is sent to the player.
Commands may be removed from display using this event, but implementations are not required to securely remove all traces of the command. If secure removal of commands is required, then the command should be assigned a permission which is not granted to the player."_

**This event is (automatically) once (and only) called when a player joins the server** - this is when the server sends the root command list to the client.  
The client stores these commands and uses them to populate the tab-completion list of the player. (The list that you see when you press the `/` key)

It is important to note that here, _root command_ means a command's main name; the name you type in right after the `/` sign.  
For example, here: `/lp editor`, the _root command_ is `lp`.

**Getting it called**
It is possible to getting it called by calling the [`Player:updateCommands()`](https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Player.html#updateCommands--) command.  
This command "Update the list of commands sent to the client."; It basically does the same as when a player joins the server.

**Tip:**  
You can modify the list of commands from the event (`getCommands()`) to remove certain root commands.

**Notes:**
- This does **not** prevent players from executing those commands, though.
- In `spigot.yml`, you can completely disable tab-completion. See [tab-complete](https://www.spigotmc.org/wiki/spigot-configuration/).
**If `tab-complete` is disabled (-1), the `PlayerCommandSendEvent` will never fire.**
- In `spigot.yml`, you can disable sending namespaced (e.g. 'minecraft:me') commands. See [send-namespaced](https://www.spigotmc.org/wiki/spigot-configuration/).

#### In-depth

The packet that server sends and which contains the list of root commands is called [`PacketPlayOutCommands, read about it here`](articles/packets/PacketPlayOutCommands.md).

<details>/
  <summary>Implementation of updateCommands()</summary>
  
In CraftBukkit, the implementation of this command [can be found](https://hub.spigotmc.org/stash/projects/SPIGOT/repos/craftbukkit/browse/src/main/java/org/bukkit/craftbukkit/entity/CraftPlayer.java#1656).  
It is basically it:  
```java
@Override
public void updateCommands() {
    if (getHandle().playerConnection == null) return;

    getHandle().server.getCommandDispatcher().a(getHandle());
}
```
`getHandle()` returns `this.entity` which is an `EntityPlayer`.

It seems like the CommandDispatcher's `a(EntityPlayer)` method is what it does the trick.  
By decompiling `spigot-1.15.2.jar`, I opened `CommandDispatcher.java` and this is the method, and also this is the only method in the class that sends out a `PacketPlayOutCommands`:
```java
  public void a(EntityPlayer entityplayer) {
    if (SpigotConfig.tabComplete < 0)
      return; 
    Map<CommandNode<CommandListenerWrapper>, CommandNode<ICompletionProvider>> map = Maps.newIdentityHashMap();
    RootCommandNode vanillaRoot = new RootCommandNode();
    RootCommandNode<CommandListenerWrapper> vanilla = entityplayer.server.vanillaCommandDispatcher.a().getRoot();
    map.put(vanilla, vanillaRoot);
    a((CommandNode<CommandListenerWrapper>)vanilla, (CommandNode<ICompletionProvider>)vanillaRoot, entityplayer.getCommandListener(), map);
    RootCommandNode<ICompletionProvider> rootcommandnode = new RootCommandNode();
    map.put(this.b.getRoot(), rootcommandnode);
    a((CommandNode<CommandListenerWrapper>)this.b.getRoot(), (CommandNode<ICompletionProvider>)rootcommandnode, entityplayer.getCommandListener(), map);
    Collection<String> bukkit = new LinkedHashSet<>();
    for (CommandNode node : rootcommandnode.getChildren())
      bukkit.add(node.getName()); 
    PlayerCommandSendEvent event = new PlayerCommandSendEvent((Player)entityplayer.getBukkitEntity(), new LinkedHashSet<>(bukkit));
    event.getPlayer().getServer().getPluginManager().callEvent((Event)event);
    for (String orig : bukkit) {
      if (!event.getCommands().contains(orig))
        rootcommandnode.removeCommand(orig); 
    } 
    entityplayer.playerConnection.sendPacket(new PacketPlayOutCommands(rootcommandnode));
  }
```
  
</details>