# Tab completion
"Tab completion" of commands has been existing for a long time.  

# Overview
## Before 1.13
Before 1.13, this feature worked like it is used to work in command lines:  
1. You open chat and type `/`.
2. From now on, you can use <TAB> anytime to get your current command completed. 
If by the given prefix there are multiple candidate commands, the list of available commands are shown in chat for you.
E.g. you press <TAB> at `/t`, you will see in chat: "`/tell, /trigger`". 
This also works at root level: if you press <TAB> after `/`, you will see the available commands for you.  

Example from 1.12.2:  
![](https://i.imgur.com/paLJKdm.png)

From the server side, I don't know how these are/were handled, and since I am focusing on latest version development, I am not deep-diving this one.  
Though, it is sure that the following types existed in 1.12.2 Spigot API as well: `PlayerChatTabCompleteEvent`, `TabCompleteEvent`, `TabCompleter`.

## From 1.13
#### Root command list
Starting from 1.13, players have better and more visual aid for commands.
- You open chat and type `/`. At this point (if you have 'Command Suggestions' settings ON), the list of available commands (roots) appear as a list, like this:  
![](https://i.imgur.com/WDH5dLp.png)

As you type (or backspace/delete), the list of commands dynamically filter based on the typed string.  
For example, for `/t`:  
![](https://i.imgur.com/OzosFGT.png)

#### Command arguments helper
But there is another kind of new tab completion help: for sub-commands/arguments.  
Example:
- Open chat, type `/trigger` and press <SPACE>.
Now, above the textbox you will see `<objective> [add|set]` visual aid that helps you with the parameterization of the given command.  
![](https://i.imgur.com/gmsGMmd.png)

# Development
The followings will be true for the 1.15.2 release of the client and `spigot-1.15.2` server.
I think this functionality haven't changed since 1.13 so it should work 1.13+.

## Configuration
In [`spigot.yml`](https://www.spigotmc.org/wiki/spigot-configuration/), there are multiple settings to configure tab completion:  
1. `tab-complete`: can be used to **completely** disable the tab completion feature. This affects both root command sending and the command argument helper, for all players on the server regardless of permissions.
2. `send-namespaced`: if set to false, does not send commands in `<plugin>:<command>` syntax to players (basically any command that has a `:` in it).

## Root command list sending
### Mechanics
When a player logs in to the server, the server sends the list of root commands (strings such as `help`, etc.) for the player,
but only those commands for which the player has **permission** to use. However, this does not work exactly as expected, because:  
- developers may not develop their plugins to use permissions _correctly_
- even if the user does not have permission for _any_ command (`-*`), the user still receives basic commands such as `me`, `tell`, `trigger`.
I don't know how this works exactly **yet**, but I will do some more in-depth research on this topic. **TODO**

### Modify
By subscribing to the [PlayerCommandSendEvent](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/player/PlayerCommandSendEvent.html),
it is possible to modify the list of "root" commands (list of strings) sent to a player when the player joins the server.
More info on that [**here**](https://gitlab.com/Arphox.HobbyProjects/minecraft/mcdevkb/-/blob/master/articles/events/PlayerCommandSendEvent.md).  
It is important to notice however that modifying the list of sent commands only affect the tab-completion list, and does **not** prevent players from using those commands.